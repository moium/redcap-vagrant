# Redcap Vagrant

[Vagrant](http://www.vagrantup.com/) configuration and install scripts for running [RedCap](http://project-redcap.org) on an Ubuntu 64 Precise image.

After performing the install steps below, you will have a running Ubuntu virtual machine with RedCap partially installed.  You will then be instructed to visit [http://localhost:1080/](http://localhost:1080/) for further instructions on how to complete the installation.

## Prerequisites

 * Install [Oracle VirtualBox](https://www.virtualbox.org/)
 * Install [Vagrant](http://www.vagrantup.com/)
 * Download the [Redcap](http://project-redcap.org/) code from the Redcap Wiki.  
   (You will need to be a member of the Redcap consortium.  See information at the Redcap website.)


## Install

~~~
$ git clone https://bitbucket.org/MCW_BMI/redcap-vagrant.git
$ cd redcap-vagrant
 (at this point please copy your redcap*.zip file into the current directory)
$ vagrant up
~~~

## Notes
 * This is intended for local development and testing only. The RedCap web application created by this process will only be available locally on the machine hosting the virtual instance.
 * The /var/www directory inside the virtual machine is shared to the local webroot directory, so you may edit files in your local operating system without needing to enter the virtual machine.
 * If using Windows (and you intend to ssh into the VM), we recommend using [Cygwin](http://www.cygwin.com/), since it is easier than working with [Putty](http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html) when it comes to setting up keys. 
 * This project was inspired by [Vivo Vagrant](https://github.com/lawlesst/vivo-vagrant)
 * The provisioning script will not erase any files in your webroot directory.  If you want to start from scratch (vagrant destroy;vagrant up), first remove all files from the local webroot directory.
