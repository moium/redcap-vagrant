#!/bin/bash

export DEBIAN_FRONTEND=noninteractive
set -e # Exit script immediately on first error.

#first test to be sure there is a redcap zip file present:
if [ ! -f /vagrant/redcap*.zip ]; then
  echo "---------------------------------------------------------------------------------------"
  echo " "
  echo "Cannot run provisioning script! Please download a redcap install zip from the redcap wiki and put it in"
  echo "the current directory (same directory where Vagrantfile is located). Then, to restart"
  echo "the install run 'vagrant reload --provision'"
  echo " "
  echo "---------------------------------------------------------------------------------------"
  exit
fi


if [ "$(ls /var/www)" ]; then
  echo "---------------------------------------------------------------------------------------"
  echo " "
  echo "Cannot run provisioning script! You already have content in your webroot directory."
  echo "Please fully remove all files in the webroot directory if you want to start from scratch."
  echo "Then, to restart the install run 'vagrant reload --provision'"
  echo " "
  echo "---------------------------------------------------------------------------------------"
  exit
fi

set -x # Print commands and their arguments as they are executed.

# Comment out during development
#sudo apt-get update -y

#Set time zone
area="America"
zone="New_York"
sudo echo "$area/$zone" > /tmp/timezone
sudo cp -f /tmp/timezone /etc/timezone
sudo cp -f /usr/share/zoneinfo/$area/$zone /etc/localtime

# Basics.
sudo apt-get install -y git-core vim wget curl unzip

# Web server
sudo apt-get install -y apache2

# MySQL
echo mysql-server mysql-server/root_password password redcap | sudo debconf-set-selections
echo mysql-server mysql-server/root_password_again password redcap | sudo debconf-set-selections
sudo apt-get install -y mysql-server
sudo apt-get install -y mysql-client

#create database
mysql -uroot -predcap -e "CREATE DATABASE IF NOT EXISTS redcap DEFAULT CHARACTER SET utf8;"

#set up postfix mail server so redcap can send mail out
#Note, your network must allow outgoing port 25 (some restrict this for security)
#If you are just emailing to local addresses, then it will probably work even if
#port 25 is blocked to outside addresses
debconf-set-selections <<< "postfix postfix/mailname string redcap-vagrant.somedomain.com"
debconf-set-selections <<< "postfix postfix/main_mailer_type string 'Internet Site'"
apt-get install -y postfix

#other stuff
sudo apt-get install -y php5 php5-cli php5-mcrypt php5-imap php5-curl php-pear php5-gd php5-mysql curl imagemagick subversion libapache2-mod-php5

#Default .bashrc to home directory
cp /home/vagrant/provision/.bashrc /home/vagrant/.

#put a few html and php files in the webroot
sudo cp /home/vagrant/provision/tableBasedAuth.html /var/www
sudo cp /home/vagrant/provision/*.php /var/www

# copy in the index.html substituting the current builddate
sudo sed -e "s/BUILDDATE/`date`/g" /home/vagrant/provision/index.html > /var/www/index.html

#set up pma
sudo wget --quiet http://sourceforge.net/projects/phpmyadmin/files/latest/download -O /tmp/phpmyadmin.zip
sudo unzip -qq -o /tmp/phpmyadmin.zip -d /var/www
sudo ln -s /var/www/phpMyAdmin* /var/www/pma

# Set up RedCap
#unzip the redcap*.zip file that the end user has placed in the vagrant root directory
sudo unzip -qq /vagrant/redcap*.zip -d /var/www
# set the db params
sudo sed -e 's/your_mysql_host_name/localhost/g' -e 's/your_mysql_db_name/redcap/g' -e 's/your_mysql_db_username/root/g' -e 's/your_mysql_db_password/redcap/g' /var/www/redcap/database.php > /tmp/database.php
# set the salt, using unix timestamp
sudo sed -e "s/salt = '';/salt = '"`date +%s`"';/g" /tmp/database.php > /tmp/database2.php
sudo cp /tmp/database2.php /var/www/redcap/database.php

sudo chmod a+rx /var/log/apache2
sudo chmod a+r /var/log/apache2/*

sudo pear install auth
sudo pear install db

# install the redcap cron job:
sudo bash -c 'cp /home/vagrant/provision/redcapcrontab /tmp/redcapcrontab'
sudo bash -c 'cat /tmp/redcapcrontab'
sudo bash -c 'crontab /tmp/redcapcrontab'

#change to recommended settings in the php.ini
sudo sed -e 's/post_max_size = 8M/post_max_size = 32M/g' -e 's/upload_max_filesize = 2M/upload_max_filesize = 32M/g' -e 's/; max_input_vars = 1000/max_input_vars = 10000/g' /etc/php5/apache2/php.ini > /tmp/php.ini
sudo cp /tmp/php.ini /etc/php5/apache2/php.ini

sudo /etc/init.d/apache2 restart

set +x 

echo "---------------------------------------------------------------------------------------"
echo " "
echo " "
echo "Your RedCap box is ready! Type \"vagrant ssh\" to log in or visit http://localhost:1080"
echo " "
echo " "
echo "---------------------------------------------------------------------------------------"

exit
